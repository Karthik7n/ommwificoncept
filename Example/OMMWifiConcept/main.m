//
//  main.m
//  OMMWifiConcept
//
//  Created by karthik1739 on 07/05/2017.
//  Copyright (c) 2017 karthik1739. All rights reserved.
//

@import UIKit;
#import "OMMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([OMMAppDelegate class]));
    }
}
