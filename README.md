# OMMWifiConcept

[![CI Status](http://img.shields.io/travis/karthik1739/OMMWifiConcept.svg?style=flat)](https://travis-ci.org/karthik1739/OMMWifiConcept)
[![Version](https://img.shields.io/cocoapods/v/OMMWifiConcept.svg?style=flat)](http://cocoapods.org/pods/OMMWifiConcept)
[![License](https://img.shields.io/cocoapods/l/OMMWifiConcept.svg?style=flat)](http://cocoapods.org/pods/OMMWifiConcept)
[![Platform](https://img.shields.io/cocoapods/p/OMMWifiConcept.svg?style=flat)](http://cocoapods.org/pods/OMMWifiConcept)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

OMMWifiConcept is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "OMMWifiConcept"
```

## Author

karthik1739, karthik@onmymobile.co

## License

OMMWifiConcept is available under the MIT license. See the LICENSE file for more info.
